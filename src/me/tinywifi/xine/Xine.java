package me.tinywifi.xine;

import me.tinywifi.xine.commands.feed;
import me.tinywifi.xine.commands.fly;
import me.tinywifi.xine.commands.heal;
import me.tinywifi.xine.commands.summon;
import me.tinywifi.xine.events.DisableTNTExplodeBlock;
import me.tinywifi.xine.events.PlayerWelcomeMessage;
import org.bukkit.ChatColor;
import org.bukkit.plugin.java.JavaPlugin;

public class Xine extends JavaPlugin {

    @Override
    public void onEnable() {
        getServer().getPluginManager().registerEvents(new PlayerWelcomeMessage(), this);
        getServer().getPluginManager().registerEvents(new DisableTNTExplodeBlock(), this);
        getCommand("heal").setExecutor(new heal());;
        getCommand("feed").setExecutor(new feed());
        getCommand("fly").setExecutor(new fly(this));
        getCommand("summon").setExecutor(new summon());
        getServer().getConsoleSender().sendMessage(ChatColor.GREEN + "[Xine]: Plugin is fully loaded!");
    }
    @Override
    public void onDisable() {
        getServer().getConsoleSender().sendMessage(ChatColor.RED + "[Xine]: Plugin is fully unloaded!");
    }

}
