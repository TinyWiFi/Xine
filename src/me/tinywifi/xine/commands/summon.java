package me.tinywifi.xine.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

public class summon implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (player.hasPermission("xine.summon")) {
                if (args.length >= 2) {
                    try {
                        EntityType entity = EntityType.valueOf(args[0].toUpperCase());
                        int amount = Integer.parseInt(args[1]);
                        for (int i = 0; i < amount; i++) {
                            player.getWorld().spawnEntity(player.getLocation(), entity);
                        }
                    } catch (IllegalArgumentException e) {
                        player.sendMessage("§c[Xine] That is not a valid entity!");
                    }
                } else {
                    player.sendMessage("§c[Xine] /summon <mob> <ammount>");
                }
            } else {
                player.sendMessage("§c(!) You do not have the require permission to run this command.");
            }

        } else {
            sender.sendMessage(ChatColor.RED + "[Xine] Only Players can use this command!");
        }

        return true;
    }
}