package me.tinywifi.xine.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.attribute.Attribute;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class heal implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (player.hasPermission("xine.heal")) {
                if (args.length == 0) {
                    double maxHealth = player.getAttribute(Attribute.GENERIC_MAX_HEALTH).getDefaultValue();
                    player.setHealth(maxHealth);
                    player.sendMessage("§e[Xine] You have been healed!");
                } else {
                    Player target = Bukkit.getPlayerExact(args[0]);
                    if (target instanceof Player) {
                        target.sendMessage("§e[Xine] You have been healed!");
                        double maxHealth = player.getAttribute(Attribute.GENERIC_MAX_HEALTH).getDefaultValue();
                        player.setHealth(maxHealth);
                        player.sendMessage("§e[Xine] You have healed " + target.getDisplayName());
                    } else {
                        player.sendMessage("§c[Xine] That Player does not exist.");
                    }
                }
            } else {
                player.sendMessage("§c(!) You do not have the require permission to run this command.");
            }
            return true;
        } else {
            sender.sendMessage(ChatColor.RED + "[Xine] Only Players can use this command!");
        }
        return true;
    }
}
