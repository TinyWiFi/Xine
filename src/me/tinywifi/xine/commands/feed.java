package me.tinywifi.xine.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class feed implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (player.hasPermission("xine.feed")) {
                if (args.length == 0) {
                    player.setFoodLevel(20);
                    player.sendMessage("§e[Xine] You have been fed!");
                } else {
                    Player target = Bukkit.getPlayerExact(args[0]);
                    if (target instanceof Player) {
                        target.sendMessage("§e[Xine] You have been fed!");
                        target.setFoodLevel(20);
                        player.sendMessage("§e[Xine] You have fed " + target.getDisplayName());
                    } else {
                        player.sendMessage("§c[Xine] That Player does not exist.");
                    }
                }
            } else {
                player.sendMessage("§c(!) You do not have the require permission to run this command.");
            }
            return true;
        } else {
            sender.sendMessage(ChatColor.RED + "[Xine] Only Players can use this command!");
        }
        return true;
    }
}