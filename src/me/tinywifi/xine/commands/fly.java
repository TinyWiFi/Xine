package me.tinywifi.xine.commands;

import me.tinywifi.xine.Xine;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;

public class fly implements CommandExecutor {

    private final ArrayList<Player> list_of_flying_people = new ArrayList<>();
    private final Xine plugin;

    public fly(Xine plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (player.hasPermission("xine.fly")) {
                if (list_of_flying_people.contains(player)) {
                    list_of_flying_people.remove(player);
                    player.sendMessage("&e[Xine] &3You can no longer fly.");
                    player.setAllowFlight(false);
                } else if (!list_of_flying_people.contains(player)) {
                    list_of_flying_people.add(player);
                    player.sendMessage("&e[Xine] &bYou can fly now. Be free!");
                    player.setAllowFlight(true);
                }
            } else {
                player.sendMessage(ChatColor.GREEN + "You don't have the " + ChatColor.YELLOW + "xine.fly " + ChatColor.GREEN + "permission required to use this command.");
            }
        } else {
            sender.sendMessage(ChatColor.RED + "[Xine] Only Players can use this command!");
        }

        return true;
    }
}